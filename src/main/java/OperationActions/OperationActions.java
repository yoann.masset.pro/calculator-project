package OperationActions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Graphics.Calculator;

/**
 * A class with all actions needed
 * 
 * @author Yoann MASSET
 */
public class OperationActions {

	/**
	 * The frame of the calculator
	 */
	private Calculator window;

	/**
	 * The constructor of the {@link OperationActions}
	 * 
	 * @param window : The frame of the calculator
	 */
	public OperationActions(Calculator window) {
		super();
		this.window = window;
	}

	/**
	 * When you want to execute an operation
	 * 
	 * @param secondNumber : the second number of the operation
	 */
	public void executeOperation(float secondNumber) {
		float res = 0;

		// Check the operation type
		if (window.getOperation().equals("+")) {
			res = executeAddition(secondNumber);
		} else if (window.getOperation().equals("-")) {
			res = executeSubtraction(secondNumber);
		} else if (window.getOperation().equals("*")) {
			res = executeMultiplication(secondNumber);
		} else if (window.getOperation().equals("/")) {
			if (secondNumber != 0) {
				res = executeDivision(secondNumber);
			} else {
				JFrame j = new JFrame();
				JOptionPane.showMessageDialog(j, "Cannot divide by zero !!", "WARNING", JOptionPane.WARNING_MESSAGE);
			}
		} else if (window.getOperation().equals("%")) {
			res = executeModulo(secondNumber);
		}

		// Display the result
		window.displayFormattedResult(String.valueOf(res));
		window.setFirstNumber(null);
		window.setOperation(null);
	}

	/**
	 * To execute an addition
	 * 
	 * @param secondNumber : the second number of the operation
	 * @return the result
	 */
	public float executeAddition(float secondNumber) {
		return Float.parseFloat(window.getFirstNumber()) + secondNumber;
	}

	/**
	 * To execute a subtraction
	 * 
	 * @param secondNumber : the second number of the operation
	 * @return the result
	 */
	public float executeSubtraction(float secondNumber) {
		return Float.parseFloat(window.getFirstNumber()) - secondNumber;
	}

	/**
	 * To execute a multiplication
	 * 
	 * @param secondNumber : the second number of the operation
	 * @return the result
	 */
	public float executeMultiplication(float secondNumber) {
		return Float.parseFloat(window.getFirstNumber()) * secondNumber;
	}

	/**
	 * To execute a division
	 * 
	 * @param secondNumber : the second number of the operation
	 * @return the result
	 */
	public float executeDivision(float secondNumber) {
		return Float.parseFloat(window.getFirstNumber()) / secondNumber;
	}

	/**
	 * To execute a modulo
	 * 
	 * @param secondNumber : the second number of the operation
	 * @return the result
	 */
	public float executeModulo(float secondNumber) {
		return Float.parseFloat(window.getFirstNumber()) % secondNumber;
	}
}
