package OperationActions;

import java.lang.Math;

/**
 * A class with all actions needed for the scientific part
 * 
 * @author Yoann MASSET
 */
public class ScientificOperations {

	/**
	 * To execute a cosine
	 * 
	 * @param number : the number
	 * @param rad    : if the cosine was in radiant or in degrees
	 * @return the result
	 */
	public static double executeCos(float number, Boolean rad) {
		if (rad)
			return Math.cos(number);
		else
			return Math.cos(number * returnPi() / 180);
	}

	/**
	 * To execute a sine
	 * 
	 * @param number : the number
	 * @param rad    : if the sine was in radiant or in degrees
	 * @return the result
	 */
	public static double executeSin(float number, Boolean rad) {
		if (rad)
			return Math.sin(number);
		else
			return Math.sin(number * returnPi() / 180);
	}

	/**
	 * To execute a tangent
	 * 
	 * @param number : the number
	 * @param rad    : if the tangent was in radiant or in degrees
	 * @return the result
	 */
	public static double executeTan(float number, Boolean rad) {
		if (rad)
			return Math.tan(number);
		else
			return Math.tan(number * returnPi() / 180);
	}

	/**
	 * Return the value of PI
	 * 
	 * @return PI
	 */
	public static double returnPi() {
		return Math.PI;
	}

	/**
	 * To execute the inverse of a number <br>
	 * The number must be different from 0
	 * 
	 * @param number : the number
	 * @return the inverse of the number
	 */
	public static float executeInverse(float number) {
		return 1 / number;
	}

	/**
	 * To execute the square of a number
	 * 
	 * @param number : the number
	 * @return the square of the number
	 */
	public static float executeSquare(float number) {
		return number * number;
	}

	/**
	 * To execute the square root of a number <br>
	 * The number must not be negative
	 * 
	 * @param number : the number
	 * @return the square root of the number
	 */
	public static float executeSqrt(float number) {
		return (float) Math.sqrt(number);
	}

	/**
	 * To execute the factorial of a number (recursive function) <br>
	 * The number must be an integer
	 * 
	 * @param number : the integer
	 * @return the factorial of the integer
	 */
	public static int executeFactorial(int number) {
		if (number == 0)
			return 1; // 0! = 1
		else
			return number * executeFactorial(number - 1); // n! = n * (n-1)!
	}
}
