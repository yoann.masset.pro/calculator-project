package Main;

import java.awt.EventQueue;

import Graphics.Calculator;

/**
 * The main class of the application
 * 
 * @author Yoann MASSET
 */
public class Main {
	/**
	 * The main method to launch the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculator frame = new Calculator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
