package Graphics;

import java.awt.Font;

/**
 * A class with the content of scientific buttons
 * 
 * @author Yoann MASSET
 */
public class ScientificButton {

	public static Font ButtonFont = new Font("Tahoma", Font.PLAIN, 14);

	public static String ScientificMode = "Scientific Mode";

	public static String ButtonCos = "Cos";
	public static String ButtonSin = "Sin";
	public static String ButtonTan = "Tan";
	public static String ButtonPi = "\u03C0";
	public static String ButtonInverse = "1/x";
	public static String ButtonSquare = "x\u00B2";
	public static String ButtonSqrt = "\u221Ax";
	public static String ButtonFactorial = "n!";

	public static String RadioButtonDeg = "DEG";
	public static String RadioButtonRad = "RAD";
}
