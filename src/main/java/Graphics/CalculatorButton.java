package Graphics;

import java.awt.Font;

/**
 * A class with the content of calculator buttons
 * 
 * @author Yoann MASSET
 */
public class CalculatorButton {

	public static Font ButtonFont = new Font("Tahoma", Font.PLAIN, 14);

	public static String Button1 = "1";
	public static String Button2 = "2";
	public static String Button3 = "3";
	public static String Button4 = "4";
	public static String Button5 = "5";
	public static String Button6 = "6";
	public static String Button7 = "7";
	public static String Button8 = "8";
	public static String Button9 = "9";
	public static String Button0 = "0";
	public static String ButtonPoint = ".";

	public static String ButtonEqual = "=";
	public static String ButtonClear = "Clear";
	public static String ButtonDelete = "DEL";

	public static String ButtonAdd = "+";
	public static String ButtonSub = "-";
	public static String ButtonMult = "*";
	public static String ButtonDiv = "/";
	public static String ButtonMod = "%";
	public static String ButtonOpposite = "+/-";
}
