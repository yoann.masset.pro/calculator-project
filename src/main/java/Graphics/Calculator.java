package Graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Listeners.*;

/**
 * A class to draw and display the frame with all its components
 * 
 * @extends {@link JFrame}
 * @author Yoann MASSET
 */
@SuppressWarnings("serial")
public class Calculator extends JFrame {

	// ================================================================================
	// MEMBERS
	// ================================================================================

	private JPanel mainPanel;
	private JTextField DisplayResult;

	private String firstNumber;
	private String operation;
	private Boolean newNumber;

	private JButton Button1;
	private JButton Button2;
	private JButton Button3;
	private JButton Button4;
	private JButton Button5;
	private JButton Button6;
	private JButton Button7;
	private JButton Button8;
	private JButton Button9;
	private JButton Button0;
	private JButton ButtonPoint;
	private JButton ButtonEqual;
	private JButton ButtonClear;
	private JButton ButtonDelete;
	private JButton ButtonAdd;
	private JButton ButtonSub;
	private JButton ButtonMult;
	private JButton ButtonDiv;
	private JButton ButtonMod;
	private JButton ButtonOpposite;

	// Scientific members

	private JCheckBox ScientificMode;
	private Boolean DisplayScientificPanel;
	private JPanel scientificPanel;

	private JButton ButtonCos;
	private JButton ButtonSin;
	private JButton ButtonTan;
	private JButton ButtonPi;
	private JButton ButtonInverse;
	private JButton ButtonSquare;
	private JButton ButtonSqrt;
	private JButton ButtonFactorial;

	private JRadioButton RadioButtonDeg;
	private JRadioButton RadioButtonRad;

	// ================================================================================
	// CONSTRUCTOR
	// ================================================================================

	/**
	 * The constructor of the {@link Calculator} class
	 */
	public Calculator() {

		firstNumber = null;
		operation = null;
		newNumber = false;

		// The frame of the application
		// --------------------------------------------------------------------------------

		setBackground(Color.DARK_GRAY);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Calculator.class.getResource("/Graphics/icon.jpg")));
		setFont(new Font("Calibri", Font.PLAIN, 14));
		setForeground(new Color(64, 64, 64));
		setTitle("Calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 420, 410);
		// setBounds(100, 100, 560, 410); // with the SCIENTIFIC PANEL

		// The main panel of the application
		// --------------------------------------------------------------------------------

		mainPanel = new JPanel();
		mainPanel.setBackground(Color.DARK_GRAY);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mainPanel.setLayout(null);
		setContentPane(mainPanel);

		// The buttons of the main panel
		// --------------------------------------------------------------------------------

		Button1 = new JButton(CalculatorButton.Button1);
		Button1.setFont(CalculatorButton.ButtonFont);
		Button1.addActionListener(new NumberButtonListener(this));
		Button1.setBounds(20, 233, 65, 51);
		mainPanel.add(Button1);

		Button2 = new JButton(CalculatorButton.Button2);
		Button2.setFont(CalculatorButton.ButtonFont);
		Button2.addActionListener(new NumberButtonListener(this));
		Button2.setBounds(95, 233, 65, 51);
		mainPanel.add(Button2);

		Button3 = new JButton(CalculatorButton.Button3);
		Button3.setFont(CalculatorButton.ButtonFont);
		Button3.addActionListener(new NumberButtonListener(this));
		Button3.setBounds(170, 233, 65, 51);
		mainPanel.add(Button3);

		Button4 = new JButton(CalculatorButton.Button4);
		Button4.setFont(CalculatorButton.ButtonFont);
		Button4.addActionListener(new NumberButtonListener(this));
		Button4.setBounds(20, 172, 65, 51);
		mainPanel.add(Button4);

		Button5 = new JButton(CalculatorButton.Button5);
		Button5.setFont(CalculatorButton.ButtonFont);
		Button5.addActionListener(new NumberButtonListener(this));
		Button5.setBounds(95, 172, 65, 51);
		mainPanel.add(Button5);

		Button6 = new JButton(CalculatorButton.Button6);
		Button6.setFont(CalculatorButton.ButtonFont);
		Button6.addActionListener(new NumberButtonListener(this));
		Button6.setBounds(170, 172, 65, 51);
		mainPanel.add(Button6);

		Button7 = new JButton(CalculatorButton.Button7);
		Button7.setFont(CalculatorButton.ButtonFont);
		Button7.addActionListener(new NumberButtonListener(this));
		Button7.setBounds(20, 111, 65, 51);
		mainPanel.add(Button7);

		Button8 = new JButton(CalculatorButton.Button8);
		Button8.setFont(CalculatorButton.ButtonFont);
		Button8.addActionListener(new NumberButtonListener(this));
		Button8.setBounds(95, 111, 65, 51);
		mainPanel.add(Button8);

		Button9 = new JButton(CalculatorButton.Button9);
		Button9.setFont(CalculatorButton.ButtonFont);
		Button9.addActionListener(new NumberButtonListener(this));
		Button9.setBounds(170, 111, 65, 51);
		mainPanel.add(Button9);

		Button0 = new JButton(CalculatorButton.Button0);
		Button0.setFont(CalculatorButton.ButtonFont);
		Button0.addActionListener(new NumberButtonListener(this));
		Button0.setBounds(95, 294, 65, 51);
		mainPanel.add(Button0);

		ButtonPoint = new JButton(CalculatorButton.ButtonPoint);
		ButtonPoint.setFont(CalculatorButton.ButtonFont);
		ButtonPoint.addActionListener(new NumberButtonListener(this));
		ButtonPoint.setBounds(170, 294, 65, 51);
		mainPanel.add(ButtonPoint);

		ButtonEqual = new JButton(CalculatorButton.ButtonEqual);
		ButtonEqual.setFont(CalculatorButton.ButtonFont);
		ButtonEqual.addActionListener(new ActionButtonListener(this));
		ButtonEqual.setBounds(245, 294, 140, 51);
		mainPanel.add(ButtonEqual);

		ButtonClear = new JButton(CalculatorButton.ButtonClear);
		ButtonClear.setFont(CalculatorButton.ButtonFont);
		ButtonClear.addActionListener(new ActionButtonListener(this));
		ButtonClear.setBounds(320, 111, 65, 51);
		mainPanel.add(ButtonClear);

		ButtonDelete = new JButton(CalculatorButton.ButtonDelete);
		ButtonDelete.setFont(CalculatorButton.ButtonFont);
		ButtonDelete.addActionListener(new ActionButtonListener(this));
		ButtonDelete.setBounds(320, 50, 65, 51);
		mainPanel.add(ButtonDelete);

		ButtonAdd = new JButton(CalculatorButton.ButtonAdd);
		ButtonAdd.setFont(CalculatorButton.ButtonFont);
		ButtonAdd.addActionListener(new ActionButtonListener(this));
		ButtonAdd.setBounds(245, 233, 65, 51);
		mainPanel.add(ButtonAdd);

		ButtonSub = new JButton(CalculatorButton.ButtonSub);
		ButtonSub.setFont(CalculatorButton.ButtonFont);
		ButtonSub.addActionListener(new ActionButtonListener(this));
		ButtonSub.setBounds(320, 233, 65, 51);
		mainPanel.add(ButtonSub);

		ButtonMult = new JButton(CalculatorButton.ButtonMult);
		ButtonMult.setFont(CalculatorButton.ButtonFont);
		ButtonMult.addActionListener(new ActionButtonListener(this));
		ButtonMult.setBounds(245, 172, 65, 51);
		mainPanel.add(ButtonMult);

		ButtonDiv = new JButton(CalculatorButton.ButtonDiv);
		ButtonDiv.setFont(CalculatorButton.ButtonFont);
		ButtonDiv.addActionListener(new ActionButtonListener(this));
		ButtonDiv.setBounds(320, 172, 65, 51);
		mainPanel.add(ButtonDiv);

		ButtonMod = new JButton(CalculatorButton.ButtonMod);
		ButtonMod.setFont(CalculatorButton.ButtonFont);
		ButtonMod.addActionListener(new ActionButtonListener(this));
		ButtonMod.setBounds(245, 111, 65, 51);
		mainPanel.add(ButtonMod);

		ButtonOpposite = new JButton(CalculatorButton.ButtonOpposite);
		ButtonOpposite.setFont(CalculatorButton.ButtonFont);
		ButtonOpposite.addActionListener(new ActionButtonListener(this));
		ButtonOpposite.setBounds(20, 294, 65, 51);
		mainPanel.add(ButtonOpposite);

		DisplayResult = new JTextField();
		DisplayResult.setEditable(false);
		DisplayResult.setFont(new Font("Tahoma", Font.PLAIN, 20));
		DisplayResult.setHorizontalAlignment(SwingConstants.RIGHT);
		DisplayResult.setText("0");
		DisplayResult.setBounds(20, 51, 290, 46);
		mainPanel.add(DisplayResult);
		DisplayResult.setColumns(10);

		// "Enter" on your keyboard => the equal button on the application
		this.getRootPane().setDefaultButton(ButtonEqual);

		// The scientific panel of the application
		// --------------------------------------------------------------------------------

		DisplayScientificPanel = false;

		ScientificMode = new JCheckBox(ScientificButton.ScientificMode);
		ScientificMode.addActionListener(new ScientificListeners(this));
		ScientificMode.setFont(ScientificButton.ButtonFont);
		ScientificMode.setBounds(20, 20, 119, 21);
		mainPanel.add(ScientificMode);

		scientificPanel = new JPanel();
		scientificPanel.setBackground(Color.DARK_GRAY);
		scientificPanel.setBounds(395, 20, 140, 325);
		scientificPanel.setLayout(null);
		// mainPanel.add(scientificPanel);

		// The buttons of the scientific panel
		// --------------------------------------------------------------------------------

		ButtonCos = new JButton(ScientificButton.ButtonCos);
		ButtonCos.addActionListener(new ScientificListeners(this));
		ButtonCos.setFont(ScientificButton.ButtonFont);
		ButtonCos.setBounds(0, 91, 65, 51);
		scientificPanel.add(ButtonCos);

		ButtonSin = new JButton(ScientificButton.ButtonSin);
		ButtonSin.addActionListener(new ScientificListeners(this));
		ButtonSin.setFont(ScientificButton.ButtonFont);
		ButtonSin.setBounds(0, 152, 65, 51);
		scientificPanel.add(ButtonSin);

		ButtonTan = new JButton(ScientificButton.ButtonTan);
		ButtonTan.addActionListener(new ScientificListeners(this));
		ButtonTan.setFont(ScientificButton.ButtonFont);
		ButtonTan.setBounds(0, 213, 65, 51);
		scientificPanel.add(ButtonTan);

		ButtonPi = new JButton(ScientificButton.ButtonPi);
		ButtonPi.addActionListener(new ScientificListeners(this));
		ButtonPi.setFont(ScientificButton.ButtonFont);
		ButtonPi.setBounds(0, 274, 65, 51);
		scientificPanel.add(ButtonPi);

		ButtonInverse = new JButton(ScientificButton.ButtonInverse);
		ButtonInverse.addActionListener(new ScientificListeners(this));
		ButtonInverse.setFont(ScientificButton.ButtonFont);
		ButtonInverse.setBounds(75, 91, 65, 51);
		scientificPanel.add(ButtonInverse);

		ButtonSquare = new JButton(ScientificButton.ButtonSquare);
		ButtonSquare.addActionListener(new ScientificListeners(this));
		ButtonSquare.setFont(ScientificButton.ButtonFont);
		ButtonSquare.setBounds(75, 152, 65, 51);
		scientificPanel.add(ButtonSquare);

		ButtonSqrt = new JButton(ScientificButton.ButtonSqrt);
		ButtonSqrt.addActionListener(new ScientificListeners(this));
		ButtonSqrt.setFont(ScientificButton.ButtonFont);
		ButtonSqrt.setBounds(75, 213, 65, 51);
		scientificPanel.add(ButtonSqrt);

		ButtonFactorial = new JButton(ScientificButton.ButtonFactorial);
		ButtonFactorial.addActionListener(new ScientificListeners(this));
		ButtonFactorial.setFont(ScientificButton.ButtonFont);
		ButtonFactorial.setBounds(75, 274, 65, 51);
		scientificPanel.add(ButtonFactorial);

		RadioButtonDeg = new JRadioButton(ScientificButton.RadioButtonDeg);
		RadioButtonDeg.addActionListener(new ScientificListeners(this));
		RadioButtonDeg.setSelected(true);
		RadioButtonDeg.setFont(ScientificButton.ButtonFont);
		RadioButtonDeg.setBounds(0, 40, 65, 21);
		scientificPanel.add(RadioButtonDeg);

		RadioButtonRad = new JRadioButton(ScientificButton.RadioButtonRad);
		RadioButtonRad.addActionListener(new ScientificListeners(this));
		RadioButtonRad.setFont(ScientificButton.ButtonFont);
		RadioButtonRad.setBounds(75, 40, 65, 21);
		scientificPanel.add(RadioButtonRad);
	}

	// ================================================================================
	// METHODS
	// ================================================================================

	/**
	 * Display the result in the JTextField of the application
	 * 
	 * @param result : the value to display
	 */
	public void displayResult(String result) {
		DisplayResult.setText(result);
	}

	/**
	 * Display the result with formatted format in the JTextField of the application
	 * <br>
	 * Example : 2.0 => 2 ; 4. => 4
	 * 
	 * @param result : the value to display
	 */
	public void displayFormattedResult(String result) {
		float temp = Float.parseFloat(result);
		if (temp % 1 == 0) {
			displayResult(String.valueOf((int) temp));
		} else {
			displayResult(result);
		}
	}

	// ================================================================================
	// GETTERS / SETTERS
	// ================================================================================

	// Getters / Setters of the main panel
	// --------------------------------------------------------------------------------

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(JPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	public JTextField getDisplayResult() {
		return DisplayResult;
	}

	public void setDisplayResult(JTextField displayResult) {
		DisplayResult = displayResult;
	}

	public String getFirstNumber() {
		return firstNumber;
	}

	public void setFirstNumber(String firstNumber) {
		this.firstNumber = firstNumber;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Boolean getNewNumber() {
		return newNumber;
	}

	public void setNewNumber(Boolean newNumber) {
		this.newNumber = newNumber;
	}

	// Getters / Setters of the buttons of the main panel
	// --------------------------------------------------------------------------------

	public JButton getButton1() {
		return Button1;
	}

	public void setButton1(JButton button1) {
		Button1 = button1;
	}

	public JButton getButton2() {
		return Button2;
	}

	public void setButton2(JButton button2) {
		Button2 = button2;
	}

	public JButton getButton3() {
		return Button3;
	}

	public void setButton3(JButton button3) {
		Button3 = button3;
	}

	public JButton getButton4() {
		return Button4;
	}

	public void setButton4(JButton button4) {
		Button4 = button4;
	}

	public JButton getButton5() {
		return Button5;
	}

	public void setButton5(JButton button5) {
		Button5 = button5;
	}

	public JButton getButton6() {
		return Button6;
	}

	public void setButton6(JButton button6) {
		Button6 = button6;
	}

	public JButton getButton7() {
		return Button7;
	}

	public void setButton7(JButton button7) {
		Button7 = button7;
	}

	public JButton getButton8() {
		return Button8;
	}

	public void setButton8(JButton button8) {
		Button8 = button8;
	}

	public JButton getButton9() {
		return Button9;
	}

	public void setButton9(JButton button9) {
		Button9 = button9;
	}

	public JButton getButton0() {
		return Button0;
	}

	public void setButton0(JButton button0) {
		Button0 = button0;
	}

	public JButton getButtonPoint() {
		return ButtonPoint;
	}

	public void setButtonPoint(JButton buttonPoint) {
		ButtonPoint = buttonPoint;
	}

	public JButton getButtonEqual() {
		return ButtonEqual;
	}

	public void setButtonEqual(JButton buttonEqual) {
		ButtonEqual = buttonEqual;
	}

	public JButton getButtonClear() {
		return ButtonClear;
	}

	public void setButtonClear(JButton buttonClear) {
		ButtonClear = buttonClear;
	}

	public JButton getButtonDelete() {
		return ButtonDelete;
	}

	public void setButtonDelete(JButton buttonDelete) {
		ButtonDelete = buttonDelete;
	}

	public JButton getButtonAdd() {
		return ButtonAdd;
	}

	public void setButtonAdd(JButton buttonAdd) {
		ButtonAdd = buttonAdd;
	}

	public JButton getButtonSub() {
		return ButtonSub;
	}

	public void setButtonSub(JButton buttonSub) {
		ButtonSub = buttonSub;
	}

	public JButton getButtonMult() {
		return ButtonMult;
	}

	public void setButtonMult(JButton buttonMult) {
		ButtonMult = buttonMult;
	}

	public JButton getButtonDiv() {
		return ButtonDiv;
	}

	public void setButtonDiv(JButton buttonDiv) {
		ButtonDiv = buttonDiv;
	}

	public JButton getButtonMod() {
		return ButtonMod;
	}

	public void setButtonMod(JButton buttonMod) {
		ButtonMod = buttonMod;
	}

	public JButton getButtonOpposite() {
		return ButtonOpposite;
	}

	public void setButtonOpposite(JButton buttonOpposite) {
		ButtonOpposite = buttonOpposite;
	}

	// Getters / Setters of the scientific panel
	// --------------------------------------------------------------------------------

	public JPanel getScientificPanel() {
		return scientificPanel;
	}

	public void setScientificPanel(JPanel scientificPanel) {
		this.scientificPanel = scientificPanel;
	}

	public Boolean getDisplayScientificPanel() {
		return DisplayScientificPanel;
	}

	public void setDisplayScientificPanel(Boolean displayScientificPanel) {
		DisplayScientificPanel = displayScientificPanel;
	}

	public JCheckBox getScientificMode() {
		return ScientificMode;
	}

	public void setScientificMode(JCheckBox scientificMode) {
		ScientificMode = scientificMode;
	}

	// Getters / Setters of the buttons of the scientific panel
	// --------------------------------------------------------------------------------

	public JButton getButtonCos() {
		return ButtonCos;
	}

	public void setButtonCos(JButton buttonCos) {
		ButtonCos = buttonCos;
	}

	public JButton getButtonSin() {
		return ButtonSin;
	}

	public void setButtonSin(JButton buttonSin) {
		ButtonSin = buttonSin;
	}

	public JButton getButtonTan() {
		return ButtonTan;
	}

	public void setButtonTan(JButton buttonTan) {
		ButtonTan = buttonTan;
	}

	public JButton getButtonPi() {
		return ButtonPi;
	}

	public void setButtonPi(JButton buttonPi) {
		ButtonPi = buttonPi;
	}

	public JButton getButtonInverse() {
		return ButtonInverse;
	}

	public void setButtonInverse(JButton buttonInverse) {
		ButtonInverse = buttonInverse;
	}

	public JButton getButtonSquare() {
		return ButtonSquare;
	}

	public void setButtonSquare(JButton buttonSquare) {
		ButtonSquare = buttonSquare;
	}

	public JButton getButtonSqrt() {
		return ButtonSqrt;
	}

	public void setButtonSqrt(JButton buttonSqrt) {
		ButtonSqrt = buttonSqrt;
	}

	public JButton getButtonFactorial() {
		return ButtonFactorial;
	}

	public void setButtonFactorial(JButton buttonFactorial) {
		ButtonFactorial = buttonFactorial;
	}

	public JRadioButton getRadioButtonDeg() {
		return RadioButtonDeg;
	}

	public void setRadioButtonDeg(JRadioButton radioButtonDeg) {
		RadioButtonDeg = radioButtonDeg;
	}

	public JRadioButton getRadioButtonRad() {
		return RadioButtonRad;
	}

	public void setRadioButtonRad(JRadioButton radioButtonRad) {
		RadioButtonRad = radioButtonRad;
	}
}
