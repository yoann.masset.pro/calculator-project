package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Graphics.Calculator;
import OperationActions.OperationActions;

/**
 * A class with the listeners associated to the action buttons
 * 
 * @implements {@link ActionListener}
 * @author Yoann MASSET
 */
public class ActionButtonListener implements ActionListener {

	/**
	 * The frame of the calculator
	 */
	private Calculator window;

	/**
	 * The constructor of the {@link ActionButtonListener}
	 * 
	 * @param window : The frame of the calculator
	 */
	public ActionButtonListener(Calculator window) {
		super();
		this.window = window;
	}

	/**
	 * Invoked when an action occurs
	 * 
	 * @param e : the event to be processed
	 */
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource(); // Get the source event
		String res = window.getDisplayResult().getText(); // Get the number displayed in the JTextField

		if (button.equals(window.getButtonClear())) { // Clear the JTextField
			window.displayResult("0");
			window.setFirstNumber(null);
			window.setOperation(null);
		} else if (button.equals(window.getButtonEqual())) { // Make the calculation
			if (window.getFirstNumber() != null && window.getOperation() != null) {
				OperationActions op = new OperationActions(window);
				op.executeOperation(Float.parseFloat(res));
			}
			window.setNewNumber(true);
		} else if (button.equals(window.getButtonDelete())) { // Delete the last digit
			int len = res.length();
			if (len == 1 || (len == 2 && res.charAt(0) == '-')) {
				window.displayResult("0");
			} else {
				String sub_res = res.substring(0, len - 1);
				if (sub_res.charAt(len - 2) == '.') {
					window.displayFormattedResult(sub_res);
				} else {
					window.displayResult(sub_res);
				}
			}
		} else if (button.equals(window.getButtonOpposite())) { // Switch to positive or negative
			Float opposite = -1 * Float.parseFloat(res);
			window.displayFormattedResult(String.valueOf(opposite));
		} else { // Add the operation
			if (window.getFirstNumber() != null && window.getOperation() != null) { // If we have to make the
																					// calculation first
				OperationActions op = new OperationActions(window);
				op.executeOperation(Float.parseFloat(res));
				res = window.getDisplayResult().getText();
			}
			window.setFirstNumber(res);
			window.setOperation(button.getText());
			window.setNewNumber(true);
		}
	}
}
