package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Graphics.Calculator;
import OperationActions.*;

/**
 * A class with the listeners associated to the scientific buttons
 * 
 * @implements {@link ActionListener}
 * @author Yoann MASSET
 */
public class ScientificListeners implements ActionListener {

	/**
	 * The frame of the calculator
	 */
	private Calculator window;

	/**
	 * The constructor of the {@link ScientificListeners}
	 * 
	 * @param window : The frame of the calculator
	 */
	public ScientificListeners(Calculator window) {
		super();
		this.window = window;
	}

	/**
	 * Invoked when an action occurs
	 * 
	 * @param e : the event to be processed
	 */
	public void actionPerformed(ActionEvent e) {
		String number = window.getDisplayResult().getText(); // Get the number displayed in the JTextField

		if (e.getSource().equals(window.getScientificMode())) { // Switch to scientific mode or not
			window.setDisplayScientificPanel(((JCheckBox) e.getSource()).isSelected());
			if (window.getDisplayScientificPanel()) { // Add the scientific panel to the frame
				window.getContentPane().add(window.getScientificPanel());
				window.setBounds(100, 100, 560, 410);
			} else { // Remove the scientific panel to the frame
				window.getContentPane().remove(window.getScientificPanel());
				window.setBounds(100, 100, 420, 410);
			}
			window.getContentPane().revalidate();
			window.getContentPane().repaint();
		} else if (e.getSource().equals(window.getRadioButtonDeg())) { // Switch to degree or radiant mode
			window.getRadioButtonRad().setSelected(!window.getRadioButtonDeg().isSelected());
		} else if (e.getSource().equals(window.getRadioButtonRad())) { // Switch to degree or radiant mode
			window.getRadioButtonDeg().setSelected(!window.getRadioButtonRad().isSelected());
		} else if (e.getSource().equals(window.getButtonCos())) { // Operation cosine
			double res = ScientificOperations.executeCos(Float.parseFloat(number),
					window.getRadioButtonRad().isSelected());
			window.displayFormattedResult(String.valueOf(res));
			window.setNewNumber(true);
		} else if (e.getSource().equals(window.getButtonSin())) { // Operation sine
			double res = ScientificOperations.executeSin(Float.parseFloat(number),
					window.getRadioButtonRad().isSelected());
			window.displayFormattedResult(String.valueOf(res));
			window.setNewNumber(true);
		} else if (e.getSource().equals(window.getButtonTan())) { // Operation tangent
			double res = ScientificOperations.executeTan(Float.parseFloat(number),
					window.getRadioButtonRad().isSelected());
			window.displayFormattedResult(String.valueOf(res));
			window.setNewNumber(true);
		} else if (e.getSource().equals(window.getButtonPi())) { // Display the value of PI
			window.displayResult(String.valueOf(ScientificOperations.returnPi()));
			window.setNewNumber(true);
		} else if (e.getSource().equals(window.getButtonInverse())) { // Operation inverse
			if (Float.parseFloat(number) != 0) {
				float res = ScientificOperations.executeInverse(Float.parseFloat(number));
				window.displayFormattedResult(String.valueOf(res));
				window.setNewNumber(true);
			} else {
				JFrame j = new JFrame();
				JOptionPane.showMessageDialog(j, "Cannot do the inverse of zero !!", "WARNING",
						JOptionPane.WARNING_MESSAGE);
			}
		} else if (e.getSource().equals(window.getButtonSquare())) { // Operation square
			float res = ScientificOperations.executeSquare(Float.parseFloat(number));
			window.displayFormattedResult(String.valueOf(res));
			window.setNewNumber(true);
		} else if (e.getSource().equals(window.getButtonSqrt())) { // Operation square root
			if (Float.parseFloat(number) >= 0) {
				float res = ScientificOperations.executeSqrt(Float.parseFloat(number));
				window.displayFormattedResult(String.valueOf(res));
				window.setNewNumber(true);
			} else {
				JFrame j = new JFrame();
				JOptionPane.showMessageDialog(j, "Cannot do the square root of a negative number !!", "WARNING",
						JOptionPane.WARNING_MESSAGE);
			}
		} else if (e.getSource().equals(window.getButtonFactorial())) { // Operation factorial
			if (Float.parseFloat(number) % 1 == 0) {
				if (Float.parseFloat(number) >= 0) {
					int res = ScientificOperations.executeFactorial(Integer.parseInt(number));
					window.displayFormattedResult(String.valueOf(res));
					window.setNewNumber(true);
				} else {
					JFrame j = new JFrame();
					JOptionPane.showMessageDialog(j, "Cannot do the factorial of a negative number !!", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JFrame j = new JFrame();
				JOptionPane.showMessageDialog(j, "Cannot do the factorial of a float number !!", "WARNING",
						JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
