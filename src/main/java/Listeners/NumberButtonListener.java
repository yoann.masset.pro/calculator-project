package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Graphics.Calculator;

/**
 * A class with the listeners associated to the number buttons
 * 
 * @implements {@link ActionListener}
 * @author Yoann MASSET
 */
public class NumberButtonListener implements ActionListener {

	/**
	 * The frame of the calculator
	 */
	private Calculator window;

	/**
	 * The constructor of the {@link NumberButtonListener}
	 * 
	 * @param window : The frame of the calculator
	 */
	public NumberButtonListener(Calculator window) {
		super();
		this.window = window;
	}

	/**
	 * Invoked when an action occurs
	 * 
	 * @param e : the event to be processed
	 */
	public void actionPerformed(ActionEvent e) {

		JButton button = (JButton) e.getSource(); // Get the source event
		String res = window.getDisplayResult().getText(); // Get the number displayed in the JTextField

		// Check the button pressed
		if (button.equals(window.getButtonPoint())) {
			if (!res.contains(".")) { // Add a point to the number
				if (window.getNewNumber()) {
					window.displayResult("0" + button.getText());
					window.setNewNumber(false);
				} else
					window.displayResult(res + button.getText());
			} else {
				JFrame j = new JFrame();
				JOptionPane.showMessageDialog(j, "Impossible to put 2 points !", "WARNING",
						JOptionPane.WARNING_MESSAGE);
			}
		} else if (window.getNewNumber() || res.equals("0")) {
			window.displayResult(button.getText());
			window.setNewNumber(false);
		} else {
			window.displayResult(res + button.getText()); // Add the digit in the JTextField
		}
	}
}
